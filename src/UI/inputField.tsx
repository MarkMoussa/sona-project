import React from "react";

export const InputField = (props: {
  type: string;
  className?: string;
  name?:string
  style?: any;
  placeholder?: string;
  onChange?: any;
  onCLick?: any;
  value?: any;
}) => {
  return (
   
      <input className={props.className}
        type={props.type}
        name={props.name}
        style={props.style}
        value={props.value}
        onChange={props.onChange}
        onClick={props.onCLick}
        placeholder={props.placeholder}
      />
  
  );
};

export const NavList = () => {
  return (
    <>
      <li className="nav__bottom-right--list-item">Home</li>
      <li className="nav__bottom-right--list-item">Rooms</li>
      <li className="nav__bottom-right--list-item">About</li>
      <li className="nav__bottom-right--list-item">Pages</li>
      <li className="nav__bottom-right--list-item">News</li>
      <li className="nav__bottom-right--list-item">Contacts</li>
    </>
  );
};

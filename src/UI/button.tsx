import React, { Children } from "react";
// import CSS from "csstype";
export const Button = (props: {
  children: React.ReactNode;
  color: string;
  backgroundColor: string;
  margin?: string;
  padding?: string;
}) => {
  return (
    <button
      className="custom-btn"
      style={{
        color: props.color,
        backgroundColor: props.backgroundColor,
        margin: props.margin,
        padding: props.padding,
      }}
    >
      {props.children}
    </button>
  );
};

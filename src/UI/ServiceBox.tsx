import { IconProps } from "@iconify/react";

export const ServiceBox = (props: {
  icon: React.ReactElement<
    IconProps,
    string | React.JSXElementConstructor<any>
  >;
  title: string;
  text: string;
}) => {
  return (
    <div className="services__grid-item mark">
      <div className="services__grid-item--icon">{props.icon}</div>
      <h3 className="services__grid-item--title">{props.title}</h3>
      <p className="services__grid-item--text">{props.text}</p>
    </div>
  );
};

import { Icon } from "@iconify/react";
export const Searchbtn = () => {
  return (
    
    <li className="nav__bottom-right--list-item">
      <Icon icon="codicon:search" />
    </li>
   
  );
};

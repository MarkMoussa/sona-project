import React from "react";
import { Icon } from "@iconify/react";
import CSS from "csstype";
export const Icons = () => {
  const iconStyle: CSS.Properties = {
    marginTop:"5px",
    marginRight:"5px",
    fontSize:"20px"
  };

  return (
   <>
      <Icon
        style={iconStyle}
        icon="cib:facebook-f"
        onClick={() => {
          console.log("facebook");
        }}
      />
      <Icon
        style={iconStyle}
        icon="akar-icons:twitter-fill"
        onClick={() => {
          console.log("twitter");
        }}
      />
      <Icon
        style={iconStyle}
        icon="fa-brands:tripadvisor"
        onClick={() => {
          console.log("tripadvisor");
        }}
      />
      <Icon
      style={iconStyle}
        icon="akar-icons:instagram-fill"
        onClick={() => {
          console.log("instagram");
        }}
      />
    </>
  );
};

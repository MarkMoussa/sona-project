import "./App.scss";
import { Navbar } from "./components/Navbar/Navbar";
import { Banner } from "./components/Banner/Banner";
import { About } from "./components/About/About";
import { Srvs } from "./components/Srvs/Srvs";
import { Footer } from "./components/Footer/Footer";

function App() {
  return (
    <div className="app">
      <Navbar />
      <Banner />
      <About />
      <Srvs />
      <Footer />
    </div>
  );
}

export default App;

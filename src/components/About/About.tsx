import React from "react";
import { Button } from "../../UI/button";
import picOne from '../../img/about-1.jpg'
import picTwo from '../../img/about-2.jpg'
export const About = () => {
  return (
    <div className="about">
      <div className="about__textBox">
        <h5 className="about__textBox-title">ABOUT US</h5>
        <div className="about__textBox-header">
          Intercontinential La Wesltake Hotel
        </div>
        <p className="about__textBox-text">
          Sona.com is a leading online accomodation site.We're passionate about
          travel.Every day, we inspire and reach millions of travelers across 90
          local websites in 41 languages. <br />
          So When it comes to booking the perfect hotel, vacation rental,
          resort, apartment,guest house or tree house , we've got you covered.
        </p>
        <div className="about__textBox-button">
          <Button
            children={"Read More"}
            color={"black"}
            backgroundColor={"transparent"}
          />
        </div>
      </div>
      <div className="about__pics">
        <img
          src={picOne}
          alt="pic 1"
          className="about__pics-1"
        />
        <img src={picTwo} alt="pic 2" className="about__pics-2" />
      </div>
    </div>
  );
};

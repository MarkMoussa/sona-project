import React from "react";
import { Icon } from "@iconify/react";
import { InputField } from "../../UI/inputField";
export const Footer = () => {
  return (
    <div className="footer">
      <div className="footer__item-1">
        <h5 className="footer__item-1--title" style={{ color: "white" }}>
          Sona <span style={{ color: "#dfa974" }}>.</span>
        </h5>
        <p className="footer__item-1--text">
          We inspire and reach millions of travelers across 90 local websites
        </p>
      </div>
      <div className="footer__item-2">
        <h6 className="footer__item-2--title">CONTACT US</h6>
        <p className="footer__item-2--phone">(12) 345 67890</p>
        <p className="footer__item-2--email">info.colorlib@gmail.com</p>
        <p className="footer__item-2--address">
          856 Corida Extension Apt. 356, Lake,
        </p>
        <p className="footer__item-2--country">United State</p>
      </div>
      <div className="footer__item-3">
        <h6 className="footer__item-3--title">NEW LATEST</h6>
        <p className="footer__item-3--text">
          Get the latest updates updates and offers
        </p>

        <div className="input-submit">
          <InputField
            className="input-submit__input"
            type={"text"}
            placeholder={"Email"}
          />

          <button className="input-submit__btn">
            <Icon icon="bi:send-fill" />
          </button>
        </div>
      </div>
    </div>
  );
};

import CSS from "csstype";
import { Icon } from "@iconify/react";
import { useState } from "react";
import { useMediaQuery } from "react-responsive";
import { Icons } from "../../UI/Icons";
import { NavList } from "../../UI/NavList";
import { Searchbtn } from "../../UI/Searchbtn";
export const Navbar = () => {
  const [hambugerOpen, setHambugerOpen] = useState(false);
  const isBigScreen = useMediaQuery({ query: "(min-width : 800px)" });

  const toggleHamburger = () => {
    setHambugerOpen(!hambugerOpen);
    console.log(hambugerOpen);
  };

  const hamStyleSmall: CSS.Properties = {
    position: "fixed",
    zIndex: 99,
    border: "none",
    fontSize: "30px",
    top: "1rem",
    right: "1rem",
    padding: "4px",
    background: "transparent",
    cursor: "pointer",
  };


  return (
    <div className="nav">
      <div className="nav__top">
        <div className="nav__top-left">
          <ul className="nav__top-left--list">
            <li className="nav__top-left--list-item">
              <span>
                <Icon icon="bxs:phone" className="nav__top-icon" />
                (12) 345 678990
              </span>
            </li>
          </ul>
        </div>
        <div className="nav__top-right">
          <ul className="nav__top-right--list">
            <li className="nav__top-right--list-item-1">
              <span>
                <Icon icon="fluent:mail-24-filled" className="nav__top-icon" />{" "}
                info.colorlib@gmail.com
              </span>
            </li>
            {/* <li className="nav__top-right--list-item-2"> */}
              <li
                className="nav__bottom-right--list-item-icons"
              
              >
                <Icons />
              </li>
            {/* </li> */}
            <button
              className="nav__top-right--list-item-3"
              onClick={() => {
                console.log("clicked");
              }}
            >
              BOOK NOW
            </button>
            <li className="nav__top-right--list-item-4">
              <Icon
                icon="fxemoji:greatbritainflag"
                style={{ marginRight: "7px" }}
              />
              EN
            </li>
          </ul>
        </div>
      </div>
      <div className="nav__bottom">
        <div className="nav__bottom-left">
          Sona<span style={{ color: "#dfa974" }}>.</span>
        </div>
        {isBigScreen ? (
          <div className="nav__bottom-right">
            <ul className="nav__bottom-right--list">
              <NavList />

              <Searchbtn />
            </ul>
          </div>
        ) : (
          hambugerOpen && (
            <div className="nav__bottom-card">
              <div
                className="nav__bottom-right"
                style={{ letterSpacing: "5px", fontSize: "20px" }}
              >
                <ul style={{ listStyle: "none" }}>
                  <li>
                    <NavList />
                  </li>
                  <li
                    className="nav__bottom-right--list-item-icons"
                   
                  >
                    <Icons />
                  </li>
                </ul>
              </div>
            </div>
          )
        )}

        <button className="nav__bottom-hamburger" onClick={toggleHamburger}>
          <i style={hamStyleSmall}>
            {!hambugerOpen ? (
              <Icon icon="clarity:menu-line" />
            ) : (
              <Icon icon="clarity:close-line" />
            )}
          </i>
        </button>
      </div>
    </div>
  );
};

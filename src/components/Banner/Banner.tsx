import React from "react";
import { Button } from "../../UI/button";
import { InputField } from "../../UI/inputField";

export const Banner = () => {
  return (
    <>
      <div className="banner">
        <div className="banner__textBox">
          <h1 className="banner__textBox-header">Sona A Luxury Hotel</h1>
          <p className="banner__textBox-text">
            Here are the best hotel booking sites, including recommendations for
            international travel and for finding low-priced hotel rooms
          </p>
          {/* <button className="banner__textBox-button">Discover Now</button> */}
          <Button children={"Discover Now"} color={"white"} backgroundColor={"transparent"} />
        </div>
        <div className="banner__form">
          <h1 className="banner__form-title">Booking Your Hotel</h1>
          <label htmlFor="checkIn" className="banner__form-label">CheckIn :</label>
          <InputField name="checkIn" className="banner__form-input" type="date" onChange={(e: any)=>e.target.value}  />
          <label htmlFor="checkOut" className="banner__form-label">CheckOut :</label>
          <InputField name="checkOut" className="banner__form-input" type="date" onChange={(e:any)=>e.target.value} />
          <label htmlFor="guests" className="banner__form-label">Guests :</label>
          <select name="guests" id="guests" className="banner__form-input">
              <option value="1">2 ADULTS</option>
              <option value="2">4 ADULTS</option>
              <option value="3">FAMILY</option>
          </select>
          <label htmlFor="room" className="banner__form-label">Room :</label>
          <select name="rooms" id="rooms" className="banner__form-input">
              <option value="1">1 ROOM</option>
              <option value="2">2 ROOMS</option>
              <option value="3">3 ROOMS</option>
          </select>
          <button className="banner__form-button">CHECK AVAILABILITY</button>
        </div>
      </div>
    </>
  );
};

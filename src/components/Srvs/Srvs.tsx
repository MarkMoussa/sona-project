import { ServiceBox } from "../../UI/ServiceBox";
import { Icon } from "@iconify/react";

export const Srvs = () => {
  return (
    <div className="services">
      <h4 className="services__title">WHAT WE DO</h4>
      <h1 className="serivces__header">Discover Our Services</h1>
      <div className="services__grid">
        <ServiceBox
          icon={<Icon icon="fa6-solid:map-location-dot" />}
          title={"Travel Plan"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
        <ServiceBox
          icon={<Icon icon="cil:restaurant" />}
          title={"Catering Service"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
        <ServiceBox
          icon={<Icon icon="icons8:bed" />}
          title={"Babysitting"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
        <ServiceBox
          icon={<Icon icon="icon-park-outline:clothes-short-sleeve" />}
          title={"Laundry"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
        <ServiceBox
          icon={<Icon icon="mdi:hours-24" />}
          title={"Hire Driver"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
        <ServiceBox
          icon={<Icon icon="ep:cold-drink" />}
          title={"Bar & Drink"}
          text={
            "  Lorem ipsum dolor sit amet consectetur adipisicing elit. Et hic nobis velit magnam amet est quia praesentium quidem distinctio sequi."
          }
        />
      </div>
    </div>
  );
};
